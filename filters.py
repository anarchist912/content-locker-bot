from telegram.ext import MessageFilter, UpdateFilter, Filters
from config import MESSAGE_TYPE_BLACKLIST, MESSAGE_TYPE_WHITELIST
import logging

class ConfigurableFilter(MessageFilter):
  def __init__(self):
    self.blacklist = set(MESSAGE_TYPE_BLACKLIST)
    self.whitelist = MESSAGE_TYPE_WHITELIST
  
  def filter(self, message):
    for content_type in self.blacklist:
      if content_type not in self.whitelist and getattr(Filters, content_type):
        if getattr(Filters, content_type).filter(message):
          return True
    return False



  