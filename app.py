from config import TOKEN, LOCK_MESSAGE, LOCK_MESSAGE_DURATION, LOCK_STATUS_UPDATES
from telegram import Update
from telegram.ext import Updater, CallbackContext, CommandHandler, MessageHandler, Filters
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

from filters import ConfigurableFilter

updater = Updater(token=TOKEN)
dispatcher = updater.dispatcher
job_queue = updater.job_queue
configurable_filter = ConfigurableFilter()


def start(update: Update, context: CallbackContext):
  context.bot.send_message(chat_id=update.effective_chat.id,
                           text="Hi! You can use me to restrict certain type of content in you Telegram groups.")
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

def lock_messages(update: Update, context: CallbackContext):
  context.bot.delete_message(update.effective_chat.id, update.message.message_id)
  mod_msg = context.bot.send_message(update.effective_chat.id, LOCK_MESSAGE, disable_notification=True)
  def delete_job(context: CallbackContext):
    context.bot.delete_message(update.effective_chat.id, context.job.context.message_id)
  job_queue.run_once(delete_job, LOCK_MESSAGE_DURATION | 45, context=mod_msg)
message_handler = MessageHandler(configurable_filter, lock_messages)
dispatcher.add_handler(message_handler)

def lock_status_updates(update: Update, context: CallbackContext):
  context.bot.delete_message(update.effective_chat.id, update.message.message_id)
status_update_handler = MessageHandler(Filters.status_update, lock_status_updates)
if LOCK_STATUS_UPDATES:
  dispatcher.add_handler(status_update_handler)

updater.start_polling()
updater.idle()